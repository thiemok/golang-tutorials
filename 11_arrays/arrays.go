package main

import (
	"fmt"
)

func main() {
	var a [3]int //int array with length 3
	a[0] = 12    // array index starts at 0
	a[1] = 78
	a[2] = 50
	fmt.Println(a)

	b := [3]int{12, 78, 50} // short hand declaration to create array
	fmt.Println(b)

	c := [3]int{12}
	fmt.Println(c)

	d := [...]int{12, 78, 50} // ... makes the compiler determine the length
	fmt.Println(d)

	e := [...]string{"USA", "China", "India", "Germany", "France"}
	f := e // a copy of a is assigned to b
	f[0] = "Singapore"
	fmt.Println("e is ", e)
	fmt.Println("f is ", f)

	g := [...]float64{67.7, 89.8, 21, 78}
	fmt.Println("length of g is", len(g))

	h := [...]float64{67.7, 89.8, 21, 78}
	for i := 0; i < len(h); i++ { //looping from 0 to the length of the array
		fmt.Printf("%d th element of h is %.2f\n", i, h[i])
	}

	k := [...]float64{67.7, 89.8, 21, 78}
	sum := float64(0)
	for i, v := range k { //range returns both the index and value
		fmt.Printf("%d the element of k is %.2f\n", i, v)
		sum += v
	}
	fmt.Println("\nsum of all elements of k", sum)
}
