package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Hello, playground")

	const a = 55 //allowed
	a = 89       //reassignment not allowed

	var b = math.Sqrt(4)   //allowed
	const c = math.Sqrt(4) //not allowed
}
