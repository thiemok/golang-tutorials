package main

import (
	"fmt"
)

type add func(a int, b int) int

func simple(a func(a, b int) int) {
	fmt.Println(a(60, 7))
}

func simple2() func(a, b int) int {
	f := func(a, b int) int {
		return a + b
	}
	return f
}

func main() {
	a := func() {
		fmt.Println("hello world first class function")
	}
	a()
	fmt.Printf("%T\n", a)

	func() {
		fmt.Println("hello world first class anon function")
	}()

	b := func(n string) {
		fmt.Println("Welcome", n)
	}
	b("Gophers")
	fmt.Printf("%T\n", b)

	var c add = func(a1 int, b1 int) int {
		return a1 + b1
	}
	s := c(5, 6)
	fmt.Println("Sum", s)

	f := func(a, b int) int {
		return a + b
	}
	simple(f)

	s2 := simple2()
	fmt.Println(s2(60, 7))
}
