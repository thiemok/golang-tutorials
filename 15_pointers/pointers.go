package main

import (
	"fmt"
)

func change(val *int) {
	*val = 55
}

func main() {
	b := 255
	a := &b
	fmt.Printf("Type of a is %T\n", a)
	fmt.Println("address of b is", a)

	fmt.Println("value of b is", *a)

	*a++
	fmt.Println("new value of b is", b)

	fmt.Println("value of a before function call is", b)
	change(a)
	fmt.Println("value of a after function call is", b)
}
