package main

import "fmt"

func main() {
	var (
		age    = 111
		height = 2
	)
	fmt.Println("my age is", age, "and my height is", height, "m")
}
