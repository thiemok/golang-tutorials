package main

import (
	"fmt"
)

func main() {
	i := 55           //int
	j := 67.8         //float64
	sum := i + int(j) //j is converted to int
	fmt.Println(sum)

	x := 10
	var y float64 = float64(x) //this statement will not work without explicit conversion
	fmt.Println("y", y)
}
