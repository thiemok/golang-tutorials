package main

import (
	"fmt"
)

func main() {
	first := "Thiemo"
	last := "Krause"
	name := first + " " + last
	fmt.Println("My name is", name)
}
