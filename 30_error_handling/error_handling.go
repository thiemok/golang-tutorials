package main

import (
	"fmt"
	"net"
	"os"
	"path/filepath"
)

func example1() {
	f, err := os.Open("/test.txt")

	if err, ok := err.(*os.PathError); ok {
		fmt.Println("File at path", err.Path, "failed to open")
		return
	}

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(f.Name(), "opened successfully")
}

func example2() {
	addr, err := net.LookupHost("golangbot123.com")
	if err, ok := err.(*net.DNSError); ok {
		if err.Timeout() {
			fmt.Println("operation timed out")
		} else if err.Temporary() {
			fmt.Println("temporary error")
		} else {
			fmt.Println("generic error: ", err)
		}
		return
	}
	fmt.Println(addr)
}

func example3() {
	files, error := filepath.Glob("[")
	if error != nil && error == filepath.ErrBadPattern {
		fmt.Println(error)
		return
	}
	fmt.Println("matched files", files)
}

func main() {
	example1()
	example2()
	example3()
}
